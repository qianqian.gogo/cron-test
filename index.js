const cron = require('node-cron');
const axios = require('axios');
require('dotenv').config();

let JWT;

login = async () => {
    // make API call
    const response = await axios.request({
        method: 'POST',
        url: `${process.env.API_HOST}/${process.env.LOGIN_API}`,
        headers: {
            'x-tenant-id': process.env.TENANT_ID,
            'x-app-name': process.env.APP_NAME
        },
        data: {
            email: process.env.USERNAME,
            password: process.env.PASSWORD
        },
        timeout: 50000,
    });

    JWT = response.data.token;
    console.log('login successfully at ', new Date().toLocaleString());
}

refreshToken = async () => {
    console.log('refresh token at ', new Date().toLocaleString());
    // make API call
    try {
        const response = await axios.request({
            method: 'POST',
            url: `${process.env.API_HOST}/${process.env.REFRESH_TOKEN_API}`,
            headers: {
                'Authorization': `Bearer ${JWT}`
            },
            timeout: 50000,
        });
    
        JWT = response.data.token;
    }
    catch (error) {
        console.log(`Error at ${new Date().toLocaleString()} > `, error);
    }
}

test = () => {
    console.log('test at ', new Date().toLocaleString());
}

// first login 
login();

cron.schedule('0 */50 * * * *', refreshToken);

// cron.schedule('0 */2 * * * *', test);